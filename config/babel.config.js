/**
 * The BabelJS transpiller configuration, it is essentially used for the JSX syntax
 * @see [The BabelJS configuration]{@link https://babeljs.io/docs/en/configuration}
 * It is implicitly used by Webpack for the bundle and Jest for the tests
 * @see [The babel-loader configuration]{@link https://webpack.js.org/loaders/babel-loader/}
 * @see [The Jest support for babel]{@link https://jestjs.io/docs/getting-started#using-babel}
 * The `package.json` file extends this configuration in order to avoid polluting the root folder
 * @see [The `extends` configuration]{@link https://babeljs.io/docs/en/options#extends}
 * @member {Object} babelConfig
 */
module.exports = {
  sourceMaps: true,
  presets: [
    '@babel/preset-react',
    '@babel/preset-env'
  ]
}
