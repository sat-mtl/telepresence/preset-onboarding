import React, { createContext } from 'react'
import { Context } from '@sat-valorisation/ui-components'

import populateStores from '~/src/populateStores'
import demoPreset from '@assets/json/demoPreset'
import demoSteps from '@assets/json/demoSteps'
import demoSceneAdvice from '@assets/json/demoSceneAdvice'

import PageLayout from '@components/PageLayout'

// @todo Debug ui-components css, it triggers the heap error
import '@sat-valorisation/ui-components/ui-components.css'

import '@styles/App.scss'

const { ThemeProvider } = Context

const stores = populateStores()

/**
 * @constant {external:react/Context} AppStoresContext - Dispatches all the App stores
 * @memberof components.App
 */
export const AppStoresContext = createContext({})

/**
  * Renders the App
*/
function App () {
  const { presetStore, stepsStore, sceneAdviceStore } = stores

  // ---------- demo setup only
  const presetModel = presetStore.makePresetModel(demoPreset)
  presetStore.setPreset(presetModel)

  const stepsModel = stepsStore.makePresetStepsModel(demoSteps)
  stepsStore.setPresetSteps(stepsModel)

  const sceneAdviceModel = sceneAdviceStore.makeSceneAdviceModel(demoSceneAdvice)
  sceneAdviceStore.setSceneAdvice(sceneAdviceModel)
  // ------------------------------------------

  return (
    <ThemeProvider value='gabrielle'>
      <AppStoresContext.Provider value={stores}>
        <PageLayout />
      </AppStoresContext.Provider>
    </ThemeProvider>
  )
}

export default App
