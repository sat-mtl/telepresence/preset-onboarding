import React from 'react'
import { Common } from '@sat-valorisation/ui-components'
import arrowLeft from '@assets/icon/arrow-left.png'

const { Button } = Common

/**
 * Renders the header of the preset
 * @selector `#PresetHeader`
 * @param {string} title - The title of the preset
 * @returns {external:react/Component} The preset header
 */
const PresetHeader = ({ title }) => {
  return (
    <header id='PresetHeader'>
      <div className='Header'>
        <Button id='ReturnButton' variant='text'>
          <img src={arrowLeft} alt='arrowLeft' />
        </Button>
        <Button id='SkipButton' variant='text'>Skip</Button>
      </div>
      <div id='PresetHeader'>
        <h3>Model: {title}</h3>
      </div>
    </header>
  )
}

export default PresetHeader
