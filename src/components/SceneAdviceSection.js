import React, { useContext } from 'react'

import ProgressBar from '@utils/ProgressBar'
import { Common } from '@sat-valorisation/ui-components'
import { AppStoresContext } from '@components/App'
import { DEFAULT_CONFIG_TITLE } from '@components/ConfigurationSection'
import '@styles/SceneAdviceSection.scss'
import { CycleEnum } from '@models/CycleEnum'

const { Button, Icon } = Common

/**
 * Renders the content of the tester container when a media output is tested
 * @selector `#TesterMediaContainerButtons`
 * @returns {external:react/Component} The content for the media output test container
 */
const TesterMediaContainerButtons = () => {
  const { sceneAdviceStore } = useContext(AppStoresContext)
  const { currentSceneAdvice } = sceneAdviceStore

  return (
    <div id='TesterMediaContainerButtons'>
      <p className='TesterMediaMsg'>{currentSceneAdvice?.testerCaption}</p>
      <div className='TesterMediaButtons'>
        <Button size='large' variant='outlined' shape='rectangle'>Send a signal</Button>
        <Button size='large' variant='outlined' disabled='true' shape='rectangle'>ok</Button>
      </div>
    </div>
  )
}

/**
 * Renders the content of the tester container when a video is tested
 * @selector `#TesterMediaContainerImg`
 * @returns {external:react/Component} The content for the video test container
 */
const TesterMediaContainerImg = () => {
  const { sceneAdviceStore } = useContext(AppStoresContext)
  const { currentSceneAdvice } = sceneAdviceStore

  return (
    <div id='TesterMediaContainerImg'>
      <img src={require(`../../assets/img/${currentSceneAdvice?.id}.png`)} alt='mediaImage' className='TesterMediaImg' />
    </div>
  )
}

/**
 * Renders the content of the tester container title
 * @selector `#TesterDevice`
 * @returns {external:react/Component} The title of the tester container
 */
const TesterMediaContainerTitle = () => {
  const { sceneAdviceStore } = useContext(AppStoresContext)
  const { currentSceneAdvice } = sceneAdviceStore

  return (
    <div className='TesterDevice'>
      <Icon className='TesterDeviceIcon' type={currentSceneAdvice?.iconType} />
      {currentSceneAdvice.title}
    </div>
  )
}

/**
 * Renders the name of the tested device
 * @selector `#TesterTitle`
 * @returns {external:react/Component} The name of the tested device
 */
const SceneTesterHeader = () => {
  const { presetStore } = useContext(AppStoresContext)
  const { currentConfiguration, configurationPageNumber } = presetStore

  return (
    <div className='TesterTitle'>
      <p>{currentConfiguration?.title || DEFAULT_CONFIG_TITLE}</p>
      <div>{configurationPageNumber}/4</div>
    </div>
  )
}

/**
 * Renders the details of the scene tester
 * @selector `#Tester`
 * @returns {external:react/Component} The details of the tester container
 */
const SceneTesterDetails = () => {
  const { presetStore, sceneAdviceStore, cycleStore } = useContext(AppStoresContext)
  const { testButtonLabel, nextConfigurationId, isVideoInputTested, isAudioInputTested, isOutputTested } = presetStore
  const onSaveHandler = () => {
    cycleStore.setCurrentCycle(CycleEnum.CONFIGURING)
    presetStore.setCurrentConfigurationId(nextConfigurationId)
    sceneAdviceStore.setCurrentSceneAdviceId(nextConfigurationId)
  }

  return (
    <div className='Tester'>
      <TesterMediaContainerTitle />
      <p className='TesterCaption'>Tester</p>
      <div className='TesterMediaContainer'>
        {isVideoInputTested && <TesterMediaContainerImg />}
        {isAudioInputTested && <div style={{ height: '2rem', width: '100%' }}><ProgressBar /></div>}
        {isOutputTested && <TesterMediaContainerButtons />}
      </div>
      <div className='TesterButtonsContainer'>
        <Button size='normal' variant='text'>Reset</Button>
        {testButtonLabel !== '' && <Button size='normal' variant='contained' onClick={onSaveHandler}>{testButtonLabel}</Button>}
      </div>
    </div>
  )
}

/**
 * Renders the advice list of the scene advice section
 * @selector `#Advice`
 * @returns {external:react/Component} The list of advices for the current configuration
 */
const SceneAdvice = () => {
  const { sceneAdviceStore } = useContext(AppStoresContext)
  const { currentSceneAdvice } = sceneAdviceStore

  return (
    <div className='Advice'>
      <div className='AdviceTitle'>Staging advice list</div>
      <ul className='AdviceList'>
        {currentSceneAdvice.adviceList?.map((item, i) => (
          <li className='AdviceItem' key={i}>{item}</li>
        ))}
      </ul>
    </div>
  )
}

/**
 * Renders the scene advice section of the preset
 * @selector `#SceneAdviceSection`
 * @returns {external:react/Component} The preset scene advice section
 */
const SceneAdviceSection = () => {
  return (
    <section id='SceneAdviceSection'>
      <SceneTesterHeader />
      <div />
      <SceneTesterDetails />
      <SceneAdvice />
    </section>
  )
}

export default SceneAdviceSection
