import React from 'react'
import { createUseStyles } from 'react-jss'

import '@styles/SchemaConnector.scss'

/**
 * Hook used to set parametrized styles
 * @see {@link https://cssinjs.org JSS library}
 */
const useStyles = createUseStyles({
  connector: {
    borderWidth: props => props.isActive === true ? '3px' : '0px',
    borderStyle: 'solid',
    backgroundColor: props => props.color,
    opacity: 0.6,
    width: props => props.isActive === true ? '1.6rem' : '2rem',
    height: props => props.isActive === true ? '1.6rem' : '2rem'
  },
  connectorWrapper: {
    backgroundColor: props => props.color,
    opacity: props => props.isActive === true ? 1 : 0.6,
    borderRadius: '100%'
  }
})

/** Renders a connector schema
 * @param {Object} props - Props that are passed to the react component
 * @returns {external:react/Component} The connector schema
*/
function SchemaConnector (props) {
  const classes = useStyles(props)

  return (
    <div className={classes.connectorWrapper}>
      <div className={`${classes.connector} SchemaConnector`}>
        <span className='ConnectorContent'>
          {props.children}
        </span>
      </div>
    </div>
  )
}

export default SchemaConnector
