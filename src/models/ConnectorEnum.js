/**
 * Enum for all connector colors
 * @readonly
 * @memberof models
 * @enum {string}
 */
const ConnectorTypeEnum = Object.freeze({
  HDMI: 'hdmi',
  USB: 'usb',
  XLR_JACK: 'xlr-jack',
  DEFAULT: 'default'
})

function toColor (connectorType) {
  switch (connectorType) {
    case ConnectorTypeEnum.HDMI:
      return '#488854'
    case ConnectorTypeEnum.USB:
      return '#8331A0'
    case ConnectorTypeEnum.XLR_JACK:
      return '#EC9717'
    default:
      return '#FFFFFF'
  }
}

export default ConnectorTypeEnum
export { toColor }
