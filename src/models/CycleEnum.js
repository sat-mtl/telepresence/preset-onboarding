
/**
 * Enum for all app states
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const CycleEnum = Object.freeze({
  TESTING: 'testing',
  CONFIGURING: 'configuring',
  INITIALIZING: 'initializing',
  CHECKING: 'checking'
})
