/**
 * Enum for the entry type of each preset
 * @readonly
 * @memberof models
 * @enum {string}
 */
export const EntryTypeEnum = Object.freeze({
  INPUT: 'input',
  OUTPUT: 'output',
  ENTRIES: 'entries'
})
