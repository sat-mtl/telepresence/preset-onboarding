import React from 'react'
import Ajv from 'ajv'
import { v4 as uuidv4 } from 'uuid'

import itemSchema from '@models/schemas/item.schema.json'
import entrySchema from '@models/schemas/entry.schema.json'
import presetSchema from '@models/schemas/preset.schema.json'

import { Common } from '@sat-valorisation/ui-components'

const { Icon } = Common

/** @todo bypass the strict mode error keyword */
const presetValidator = new Ajv({
  schemas: [
    itemSchema,
    entrySchema,
    presetSchema
  ],
  strict: false
})

/**
 * @classdesc Model used to create a new preset model from a JSON object
 * @memberof models
 */
class Preset {
  /**
   * Instantiates a new preset model
   * @param {string} id - ID of the preset
   * @param {Object} deviceList - Array of the required devices
   * @param {Object} input - An object containing information of all input resources and the respective devices
   * @param {Object} output - An object containing information of all output resources and the respective devices
   * @param {Object} checkList - Array of the required items to check
   */
  constructor (id, deviceList, input, output, checkList) {
    this.id = id
    this.deviceList = deviceList
    this.input = input // <-- is this a Resource.js model
    this.output = output
    this.checkList = checkList
  }

  /**
   * Checks if an entry is an input
   * @param {string} entryId - Id of the entry
   * @returns {boolean} Returns true if the entry represents an input
   */
  isInput (entryId) {
    const isInput = this.input.resources?.some(entry => entry.id === entryId)
    return isInput
  }

  /**
     * Checks if an entry is an output
     * @param {string} entryId - Id of the entry
     * @returns {boolean} Returns true if the entry represents an output
     */
  isOutput (entryId) {
    const isOutput = this.output.resources?.some(entry => entry.id === entryId)
    return isOutput
  }

  /**
  * Makes an array of the ids of input resources
  * @returns {Object} allInputIds - An array of the available input ids
  */
  get allInputIds () {
    const { resources: inputResources } = this.input
    return inputResources.map(resource => resource.id)
  }

  /**
  * Makes an array of the ids of output resources
  * @returns {Object} allOutputIds - An array of the available output ids
  */
  get allOutputIds () {
    const { resources: outputResources } = this.output
    return outputResources.map(resource => resource.id)
  }

  /**
  * Makes an array of the ids of input and output resources
  * @returns {Object} allEntriesIds - An array of the available entries ids
  */
  get allEntriesIds () {
    return [...this.allInputIds, ...this.allOutputIds]
  }

  /**
  * Gets all input connectors of the selected preset
  * @returns {Object} allInputConnectors - An array of the available preset connectors for input
  */
  get allInputConnectors () {
    const allInputConnectors = this.input.connectors.filter(
      connector => !['NDI', 'B1'].includes(connector.label))
    return allInputConnectors
  }

  /**
  * Gets all output connectors of the selected preset
  * @returns {Object} allOutputConnectors - An array of the available preset connectors for output
  */
  get allOutputConnectors () {
    const allOutputConnectors = this.output.connectors.filter(
      connector => !['NDI', 'B1'].includes(connector.label))
    return allOutputConnectors
  }

  /**
  * Makes the input video connections options
  * @returns {Object} inputVideoConnections - An array of the available video connections for input
  */
  get inputVideoConnections () {
    const { connectors } = this.input
    const inputVideoConnections = connectors.filter(connector => connector.media === 'video')
      .map(option => ({
        id: option.label,
        label: () => (
          <span className={`${option.title.toLowerCase()}-label Label`}>
            {!['NDI', 'B1'].includes(option.label) && option.label}
            {option.label === 'NDI' && <Icon type='ndi' />}
            {option.label === 'B1' && <Icon type='bluetooth' />}
          </span>
        ),
        value: option.title
      }))
    return inputVideoConnections
  }

  /**
  * Makes the input video devices options
  * @returns {Object} inputVideoDevices - An array of the available video devices for input
  */
  get inputVideoDevices () {
    const { devices } = this.input
    const inputVideoDevices = devices.filter(device => device.media === 'video')
      .map(option => ({
        id: option.label,
        label: () => (
          <span className={`${option.title.toLowerCase()}-label Label`}>
            {option.label === 'Camera' && <Icon type='video' />}
            {option.label === 'Smartphone' && <Icon type='smartphone' />}
          </span>
        ),
        value: option.title
      }))

    return inputVideoDevices
  }

  /**
  * Makes the input audio devices options
  * @returns {Object} inputAudioDevices - An array of the available audio devices for input
  */
  get inputAudioDevices () {
    const { devices } = this.input
    const inputAudioDevices = devices.filter(device => device.media === 'audio')
      .map(option => ({
        id: option.label,
        label: () => (
          <span className={`${option.title.toLowerCase()}-label Label`}>
            {option.label === 'Microphone' && <Icon type='mic' />}
          </span>
        ),
        value: option.title
      }))

    return inputAudioDevices
  }

  /**
  * Makes the output video devices options
  * @returns {Object} outputVideoDevices - An array of the available video devices for output
  */
  get outputVideoDevices () {
    const { devices } = this.output
    const outputVideoDevices = devices.filter(device => device.media === 'video')
      .map(option => ({
        id: option.label,
        label: () => (
          <span className={`${option.title.toLowerCase()}-label Label`}>
            {option.label === 'Monitor' && <Icon type='monitor' />}
          </span>
        ),
        value: option.title
      }))

    return outputVideoDevices
  }

  /**
  * Makes the output audio devices options
  * @returns {Object} outputAudioDevices - An array of the available audio devices for output
  */
  get outputAudioDevices () {
    const { devices } = this.output
    const outputAudioDevices = devices.filter(device => device.media === 'audio')
      .map(option => ({
        id: option.label,
        label: () => (
          <span className={`${option.title.toLowerCase()}-label Label`}>
            {option.label === 'Speaker' && <Icon type='speaker' />}
          </span>
        ),
        value: option.title
      }))

    return outputAudioDevices
  }

  /**
   * Parses a JSON element to a new preset model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let preset = null
    const isValid = presetValidator.validate(presetSchema, json)

    if (isValid) {
      const id = uuidv4()
      const { deviceList, input, output, checkList } = json
      preset = new Preset(id, deviceList, input, output, checkList)
    } else {
      throw new Error(presetValidator.errorsText())
    }
    return preset
  }
}

export default Preset
