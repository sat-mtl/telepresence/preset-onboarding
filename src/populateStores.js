import PresetStore from '@stores/PresetStore'
import StepsStore from '@stores/StepsStore'
import SceneAdviceStore from '@stores/SceneAdviceStore'
import ConnectorStore from '@stores/ConnectorStore'
import CycleStore from '@stores/CycleStore'
import SceneStore from '@stores/SceneStore'

/**
 * Creates all the app's Stores
 * @returns {object} Object with all stores used by Scenic
 */
function populateStores () {
  // Preset Stores
  const cycleStore = new CycleStore()
  const presetStore = new PresetStore(cycleStore)
  const stepsStore = new StepsStore()
  const sceneAdviceStore = new SceneAdviceStore()
  const connectorStore = new ConnectorStore(presetStore, cycleStore)
  const sceneStore = new SceneStore(presetStore, connectorStore, cycleStore)

  return {
    presetStore,
    stepsStore,
    sceneAdviceStore,
    connectorStore,
    cycleStore,
    sceneStore
  }
}

export default populateStores
