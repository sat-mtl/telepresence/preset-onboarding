import SceneAdvice from '@models/SceneAdvice'
import { makeObservable, observable, action, computed } from 'mobx'

/**
 * @classdesc Stores all scene advice details for the preset
 * @memberof stores
 */
class SceneAdviceStore {
  sceneAdvice = null
  currentSceneAdviceId = 'animatorVideo'
  currentIconType = 'audio'

  constructor () {
    makeObservable(this, {
      sceneAdvice: observable,
      setSceneAdvice: action,

      currentSceneAdviceId: observable,
      setCurrentSceneAdviceId: action,

      currentIconType: observable,
      setCurrentIconType: action,

      currentSceneAdvice: computed
    })
  }

  /** @property {Object} currentSceneAdvice - The current scene advice of the preset */
  get currentSceneAdvice () {
    let currentSceneAdvice = null

    if (this.sceneAdvice) {
      for (const detail of this.sceneAdvice.details) {
        if (detail.id === this.currentSceneAdviceId) {
          currentSceneAdvice = detail
        }
      }
    }
    return currentSceneAdvice
  }

  /**
  * Creates a scene advice model from a JSON object
  * @param {Object} json - JSON representation of the scene advice
  * @returns {module:models/preset} A scene advice model
  */
  makeSceneAdviceModel (json) {
    let sceneAdvice = null

    try {
      sceneAdvice = SceneAdvice.fromJSON(json)
    } catch (error) {
      console.error(`Failed to make a scene advice model from ${json}`)
    }
    return sceneAdvice
  }

  /**
   * Sets the new sceneAdvice
   * @param {Object} preset - The scene advice model
   */
  setSceneAdvice (sceneAdvice) {
    this.sceneAdvice = sceneAdvice
  }

  /**
   * Sets the new scene advice Id
   * @param {string} itemId - The Id of the current scene advice
   */
  setCurrentSceneAdviceId (itemId) {
    this.currentSceneAdviceId = itemId
  }

  /**
   * Sets the new scene advice icon type
   * @param {string} iconType - The icon type of the current scene advice
   */
  setCurrentIconType (iconType) {
    this.currentIconType = iconType
  }
}

export default SceneAdviceStore
