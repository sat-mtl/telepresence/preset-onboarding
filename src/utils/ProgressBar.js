import React from 'react'
import '@styles/ProgressBar.scss'

/**
 * Renders the progress bar for sound testing
 * @selector `#ProgressBarContainer`
 * @returns {external:react/Component} A progress bar
 * @todo - Pass arguments to make the bar dynamic when testing
 */
const ProgressBar = () => {
  return (
    <div className='ProgressBarContainer' style={{ backgroundColor: '#D8D8D8' }}>
      <div className='ProgressBarFiller' />
    </div>
  )
}

export default ProgressBar
